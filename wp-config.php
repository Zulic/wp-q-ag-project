<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-q-ag-project' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'S1eUL):Y/z*lTExBZqFke@n5,/KrCsp0-n|&!<A5?rb,pF{c}]Ec)jY{DvOlbQ;R' );
define( 'SECURE_AUTH_KEY',  'c.i~7@Sz)G7Vt3+/@N4`l2<1d8H!$cj:0^8=:NRCf&3sK.bG,S2ip=;9OOtSt|1|' );
define( 'LOGGED_IN_KEY',    ' &A2~=dW,Z/v3ZR-Z<|k0dNnjXQ0!`h1u[OpEL##;iR)FV0G!RWQMw{ #<^|)@+[' );
define( 'NONCE_KEY',        '}zTRf5 u~ T[Tj5`9| w7YDi>A_8oW~6,*VBlf49wcptV/+h!6Sx&~;Zh9j<bd:r' );
define( 'AUTH_SALT',        'XJ|)]QpvC=!c7xn`qb6n uHhVYY|6]`xZid:Ji`YE9=2GgxLm4Ifw<|H-=+,.j`W' );
define( 'SECURE_AUTH_SALT', 'B+(tY4D G3SY1}~bJF3`QxS<H(=HH]6t]HLhUM~3A)MfIlR%5[@w8(m/=Zo6q}NJ' );
define( 'LOGGED_IN_SALT',   'BUNjAOF14bC35g22zB3K8jF_Tl3r<XkJKPvNwxp9t_ Qw)Der}FyTUz`NTgK-:97' );
define( 'NONCE_SALT',       '{P+4[[/Xx*H+H}sjI>wK@&/|)H#*ruz 0CAsUu4$.*z.)b@~I<VLUEnm<)1#If;z' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
