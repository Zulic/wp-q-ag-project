<?php
/* 
        Template Name: Movies template 
        Template Post Type: movies 
    */
get_header();
?>
<style>
    .margin {
        margin: 50px;
    }
</style>

<div id="primary">
    <div id="content" role="main" class="margin">
        <header>
            <?php the_title('<h4>', '</h4>'); ?>
        </header>
        <div>
            <?php the_meta(); ?>
            <?php the_content(); ?>
        </div>
    </div>
</div>

<?php
get_footer();
?>