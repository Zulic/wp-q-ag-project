<style>
    .movies_form {
        display: grid;
        grid-template-columns: max-content;
        grid-column-gap: 20px;
        grid-row-gap: 15px;
    }

    .movies_field {
        display: contents;
    }

    .movies_field label {
        text-transform: capitalize;
    }
</style>

<div class="movies_form">
    <div class="movies_field">
        <label for="movie_title">Movie title</label>
        <input type="text" id="movie_title" name="movie_title" value="<?php echo esc_attr(get_post_meta(get_the_ID(), 'movie_title', true)); ?>">
    </div>
</div>