<?php

/**
 * Plugin name: Movies plugin
 * Description: WP Movie Plugin for post movies
 * Author: Vedim
 * Date: 13.12.2022
 * Version: 1.0
 */

function create_post_type()
{
    register_post_type(
        'movies',
        array(
            'labels' => array(
                'name' => __('Movies'),
                'singular_name' => __('Movie'),
                'all_items' => __('All Movies'),
                'add_new' => __("Add new movie"), 
                'search_items' => __('Search Movie'),
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'movies'),
            'show_in_rest' => true,
            'supports' => array('editor', 'custom-fields'),
            'menu_position' => 5,
            'menu_icon' => 'dashicons-format-video',
        )
    );
}
add_action('init', 'create_post_type');

function custom_post_type()
{
    // Labels for Custom Post Type
    $labels = array(
        'name'                => _x('Movies', 'Post Type General Name', 'twentytwentythree'),
        'singular_name'       => _x('Movie', 'Post Type Singular Name', 'twentytwentythree'),
        'menu_name'           => __('Movies', 'twentytwentythree'),
        'parent_item_colon'   => __('Parent Movie', 'twentytwentythree'),
        'all_items'           => __('All Movies', 'twentytwentythree'),
        'view_item'           => __('View Movie', 'twentytwentythree'),
        'add_new_item'        => __('Add New Movie', 'twentytwentythree'),
        'add_new'             => __('Add New', 'twentytwentythree'),
        'edit_item'           => __('Edit Movie', 'twentytwentythree'),
        'update_item'         => __('Update Movie', 'twentytwentythree'),
        'search_items'        => __('Search Movie', 'twentytwentythree'),
        'not_found'           => __('Not Found', 'twentytwentythree'),
        'not_found_in_trash'  => __('Not found in Trash', 'twentytwentythree'),
    );
    // Set other options for Custom Post Type
    $args = array(
        'label'               => __('movies', 'twentytwentythree'),
        'description'         => __('Movie news and reviews', 'twentytwentythree'),
        'labels'              => $labels,
        'supports'            => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        'taxonomies'          => array('genres'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
    );
    // Registering your Custom Post Type
    register_post_type('movies', $args);
}
add_action('init', 'custom_post_type', 0);

// Custom fields
function register_custom_filed()
{
    add_meta_box('custom-field', 'Custom movie data', 'movie_field_display', 'movies');
}

add_action('add_meta_boxes', 'register_custom_filed');

function movie_field_display()
{
    include plugin_dir_path(__FILE__) . 'wp-movies-plugin-form.php';
}

function save_custom_filed($post_id){
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
            return;
        }
    
    if($parent_id = wp_is_post_revision($post_id)){
        $post_id = $parent_id;
    }

    if(isset($_POST['movie_title']))
    {
        $movie_title = $_POST['movie_title'];
    }else{
        $movie_title = '';
    }
    update_post_meta($post_id, 'movie_title', sanitize_text_field($movie_title));
}
add_action('save_post', 'save_custom_filed');

?>